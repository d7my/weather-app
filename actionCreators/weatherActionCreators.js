// @flow
import { paramsToQueryParams } from "../utils/url";
import * as actions from "../constants/actions";
import { BASE_URL } from "../constants/config";

const generatePromise = async (params, urlConfigs) => {
  const queryParams = paramsToQueryParams({ ...params, ...urlConfigs });
  const response = await fetch(`${BASE_URL}?&${queryParams}`);

  if (response.status >= 200 && response.status < 400) {
    const contentType = response.headers.get("Content-Type");

    if (contentType && contentType.includes("application/json")) {
      return response.json();
    }
    return response.text();
  }
};

export const fetchWeather = (params: {
  [key: string]: string | number,
}): Function => {
  return function (dispatch, getState, { urlConfigs }) {
    return dispatch({
      type: actions.FETCH_WEATHER,
      payload: generatePromise(params, urlConfigs),
    });
  };
};
