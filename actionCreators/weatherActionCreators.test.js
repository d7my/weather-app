import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import promiseMiddleware from "redux-promise-middleware";
import { fetchWeather } from "./weatherActionCreators";
import { FULFILLED, PENDING } from "../redux/reduxUtils";
import * as actions from "../constants/actions";

const payload = { list: [] };

global.fetch = jest.fn(() =>
  Promise.resolve({
    status: 200,
    headers: {
      get: () => "application/json",
    },
    json: () => Promise.resolve(payload),
  })
);

const middleWares = [
  promiseMiddleware,
  thunk.withExtraArgument({ urlConfigs: {} }),
];
const mockStore = configureMockStore(middleWares);
const store = mockStore({ weather: [] });

describe("weather action creators", () => {
  beforeEach(() => {
    fetch.mockClear();
  });

  describe("when fetching weather", () => {
    beforeEach(() => {
      store.clearActions();
    });

    it("dispatches PENDING action", async () => {
      await store.dispatch(fetchWeather());
      const action = store.getActions()[0];
      expect(action.type).toEqual(PENDING(actions.FETCH_WEATHER));
    });

    it("dispatches FULFILLED action", async () => {
      await store.dispatch(fetchWeather());
      const action = store.getActions()[1];
      expect(action.type).toEqual(FULFILLED(actions.FETCH_WEATHER));
      expect(action.payload).toEqual(payload);
    });
  });
});
