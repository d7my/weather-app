// @flow
export const PENDING = (action: string): string => `${action}_PENDING`;
export const FULFILLED = (action: string): string => `${action}_FULFILLED`;
export const REJECTED = (action: string): string => `${action}_REJECTED`;
