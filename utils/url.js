// @flow

type Params = { [key: string]: string };

export const paramsToQueryParams = (params?: Params): string => {
  if (!params) return "";

  const searchParams = new URLSearchParams();

  Object.keys(params).forEach((key) => {
    if (key) searchParams.append(key, params[key]);
  });

  return searchParams.toString();
};
