// @flow
import * as React from "react";
import styled from "styled-components";
import Link from "next/link";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { ThemeProvider } from "@material-ui/styles";
import Header from "../components/Header";
import Loader from "../components/Loader";
import GlobalStyle from "../components/GlobalStyle";
import { useStore } from "../store/store";
import { initialState } from "../reducers/initialState";
import { $theme } from "../styles/theme";

type WeatherAppProps = {|
  Component: React.ComponentType<mixed>,
  pageProps: mixed,
|};

const WeatherApp = (props: WeatherAppProps): React.Element<any> => {
  const { Component, pageProps } = props;
  const store = useStore(initialState);

  return (
    <>
      <link
        href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,500,600&display=swap"
        rel="stylesheet"
      />
      <GlobalStyle />
      <Provider store={store}>
        <ThemeProvider theme={$theme}>
          <Header />
          <Component {...pageProps} />
        </ThemeProvider>
      </Provider>
    </>
  );
};

export default WeatherApp;
