# WEATHER APP

Weather App shows temperature for the next few days [DEMO](https://weather-app-sand.vercel.app/).


### File structure:

```
├── actionCreators
├── components
├── constants
├── containers
├── node_modules
├── pages
├── reducers
├── store
├── styles
└── utils
```


### App features:
- It contains `<Carousel />` component which handles presenting cards based on view port.
- Carousel cards could be controlled by Left/Right keyboard keys.


### Development conventions:

- File names should represent what is exported.

Example:
`<Button />` component should have `Button.js` as a filename.

```jsx
const Button = () => (
  <button>
    Submit
  </button>
);

export default Button;
```

- File names should should have a `postfix` describes the content of the file to avoid looking at the path to determine whether it's a action creator file or a reducer.

Example:

```
- weatherActionCreators
- weatherReducers
```

- Constants should be in upper-snake case.

Example:

```jsx
const APP_ID = 'a24735692';
```

- Shared components should be under `/components` directory.


- Page-specific components should be under `/Page/components` directory.


### Testing:

- I used `jest` as a test runner;
- I used `@testing-library/react` to test component behaviours.
- I used `data-testid` as a testing selector which should be removed in production build using `babel-plugin-jsx-remove-data-test-id` or similar babel plugins.
- I used `` Action

#### Running tests:

- `yarn test`



### How to run the app:

- `git clone git@bitbucket.org:d7my/weather-app.git`
- `cd weather-app`
- `yarn`
- `yarn dev`
