import { combineReducers } from "redux";
import weatherReducer, { type WeatherState } from "./weatherReducer";

const reducers = combineReducers({
  weather: weatherReducer,
});

export default reducers;
