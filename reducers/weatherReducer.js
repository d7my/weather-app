// @flow
import * as actions from "../constants/actions";
import { PENDING, FULFILLED } from "../utils/reduxUtils";

export type WeatherState = {
  isLoading: boolean,
  days: string[],
};

export const weatherInitialState: WeatherState = {
  isLoading: false,
  currentCity: null,
  days: [],
};

const weatherReducer = (
  state: WeatherState = weatherInitialState,
  action: { type: string, payload: { city: string, list: Object[] } }
): WeatherState => {
  switch (action.type) {
    case PENDING(actions.FETCH_WEATHER): {
      return {
        ...state,
        isLoading: true,
      };
    }
    case FULFILLED(actions.FETCH_WEATHER): {
      return {
        ...state,
        isLoading: false,
        currentCity: action.payload.city,
        days: action.payload.list,
      };
    }
    default:
      return state;
  }
};

export default weatherReducer;
