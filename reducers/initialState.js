// @flow
import { weatherInitialState, type WeatherState } from "./weatherReducer";

export type ApplicationState = {
  weather: WeatherState,
};

export const initialState: ApplicationState = {
  weather: weatherInitialState,
};
