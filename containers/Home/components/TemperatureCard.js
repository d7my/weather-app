// @flow
import * as React from "react";
import styled from "styled-components";
import CardContent from "@material-ui/core/CardContent";
import {
  CardHeaderContainer,
  CardHeader,
  CardSubHeader,
  CardBody,
  CardBodySecondaryText,
} from "../../../components/Card";

type TemperatureCardContentProps = {|
  item: string,
  cardProps: {
    city: string,
    country: string,
    groupedByDay: { [key: string]: Object[] },
  },
|};

const round = (num) => Math.round(num);

const TemperatureText = styled.span`
  position: relative;

  &:after {
    content: "°";
    position: absolute;
    top: 0;
  }
`;

const TemperatureCardContent = (
  props: TemperatureCardContentProps
): React.Element<typeof CardContent> => {
  const date = props.item;
  const day = props.cardProps.groupedByDay[date][0];
  const description = day.weather[0].description;

  return (
    <CardContent data-testid="temperature-card-content">
      <CardHeaderContainer>
        <CardHeader data-testid="temperature-card-location">
          {props.cardProps.city}, {props.cardProps.country}
        </CardHeader>
        <CardSubHeader data-testid="temperature-card-date">
          {date}
        </CardSubHeader>
      </CardHeaderContainer>

      <CardBody>
        <CardBodySecondaryText data-testid="temperature-card-description">
          {description}
        </CardBodySecondaryText>
        <TemperatureText data-testid="temperature-card-current-temperature">
          {round(day.main.temp)}
        </TemperatureText>
        <div>
          <CardBodySecondaryText>
            <TemperatureText data-testid="temperature-card-max-temperature">
              H: {round(day.main.temp_max)}
            </TemperatureText>
            <TemperatureText data-testid="temperature-card-min-temperature">
              L: {round(day.main.temp_min)}
            </TemperatureText>
          </CardBodySecondaryText>
        </div>
      </CardBody>
    </CardContent>
  );
};

export default TemperatureCardContent;
