import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Filters from "./Filters";

const clickHandler = jest.fn();
const onFilterClick = jest.fn(() => clickHandler);
const baseProps = {
  onFilterClick,
};

const renderComponent = (props = baseProps) => render(<Filters {...props} />);

describe("<Filters />", () => {
  describe("when rendering", () => {
    beforeEach(() => {
      renderComponent();
    });

    it("renders filters container", () => {
      const filtersContainerElm = screen.getByTestId("filters-container");
      expect(filtersContainerElm).toBeInTheDocument();
    });

    it("renders filters buttons", () => {
      const filtersButtonMetricElm = screen.getByTestId(
        "filters-button-metric"
      );
      const filtersButtonImperialElm = screen.getByTestId(
        "filters-button-imperial"
      );
      expect(filtersButtonMetricElm).toBeInTheDocument();
      expect(filtersButtonImperialElm).toBeInTheDocument();
    });
  });

  describe("when clicking filters buttons", () => {
    beforeEach(() => {
      renderComponent();
      clickHandler.mockClear();
    });

    it("call onClick handler", () => {
      const filtersButtonMetricElm = screen.getByTestId(
        "filters-button-metric"
      );

      fireEvent.click(filtersButtonMetricElm, {});

      expect(clickHandler).toHaveBeenCalledTimes(1);
    });

    it("call onClick handler", () => {
      const filtersButtonImperialElm = screen.getByTestId(
        "filters-button-metric"
      );

      fireEvent.click(filtersButtonImperialElm, {});

      expect(clickHandler).toHaveBeenCalledTimes(1);
    });
  });
});
