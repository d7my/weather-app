// @flow
import * as React from "react";
import styled, { type StyledComponent } from "styled-components";
import Button from "@material-ui/core/Button";

type FiltersProps = {|
  onFilterClick: (value: string) => () => void,
|};

const FiltersContainer: StyledComponent<{||}, *, HTMLDivElement> = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-bottom: 24px;

  & > button {
    margin: 0 8px;
    box-shadow: 0 1px 3px 0 rgba(63, 63, 68, 0.1);
  }
`;

const Filters = (
  props: FiltersProps
): React.Element<typeof FiltersContainer> => (
  <FiltersContainer data-testid="filters-container">
    SHOW IN:
    <Button
      variant="contained"
      color="primary"
      data-testid="filters-button-metric"
      onClick={props.onFilterClick("Metric")}
    >
      Celsius
    </Button>
    <Button
      variant="contained"
      color="primary"
      data-testid="filters-button-imperial"
      onClick={props.onFilterClick("Imperial")}
    >
      Fahrenheit
    </Button>
  </FiltersContainer>
);

export default Filters;
