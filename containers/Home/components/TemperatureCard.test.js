import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import TemperatureCard from "./TemperatureCard";

const date = "2020-10-21";
const city = "Munich";
const country = "DE";
const description = "Cloudy";
const roundedCurrentTemp = 8;
const roundedMaxTemp = 9;
const roundedMinTemp = 8;

const baseProps = {
  item: date,
  cardProps: {
    city,
    country,
    groupedByDay: {
      [date]: [
        {
          weather: [
            {
              description,
            },
          ],
          main: {
            temp: 8.1,
            temp_max: 8.5,
            temp_min: 7.5,
          },
        },
      ],
    },
  },
};
const renderComponent = (props = baseProps) =>
  render(<TemperatureCard {...props} />);

describe("<TemperatureCard />", () => {
  beforeEach(() => {
    renderComponent();
  });

  describe("when rendering", () => {
    it("renders card content", () => {
      const cardContentElm = screen.getByTestId("temperature-card-content");
      expect(cardContentElm).toBeInTheDocument();
    });

    it("renders card location", () => {
      const cardLocationElm = screen.getByTestId("temperature-card-location");
      expect(cardLocationElm).toBeInTheDocument();
      expect(cardLocationElm.innerHTML).toEqual(`${city}, ${country}`);
    });

    it("renders current date", () => {
      const cardDateElm = screen.getByTestId("temperature-card-date");
      expect(cardDateElm).toBeInTheDocument();
      expect(cardDateElm.innerHTML).toEqual(date);
    });

    it("renders card description", () => {
      const cardDescriptionElm = screen.getByTestId(
        "temperature-card-description"
      );
      expect(cardDescriptionElm).toBeInTheDocument();
      expect(cardDescriptionElm.innerHTML).toEqual(description);
    });

    it("renders current temperature", () => {
      const cardCurrentTempElm = screen.getByTestId(
        "temperature-card-current-temperature"
      );
      expect(cardCurrentTempElm).toBeInTheDocument();
      expect(cardCurrentTempElm.innerHTML).toEqual(`${roundedCurrentTemp}`);
    });

    it("renders max temperature", () => {
      const cardMaxTempElm = screen.getByTestId(
        "temperature-card-max-temperature"
      );
      expect(cardMaxTempElm).toBeInTheDocument();
      expect(cardMaxTempElm.innerHTML).toEqual(`H: ${roundedMaxTemp}`);
    });

    it("renders min temperature", () => {
      const cardMinTempElm = screen.getByTestId(
        "temperature-card-min-temperature"
      );
      expect(cardMinTempElm).toBeInTheDocument();
      expect(cardMinTempElm.innerHTML).toEqual(`L: ${roundedMinTemp}`);
    });
  });
});
