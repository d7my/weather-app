import * as React from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import Page from "../../components/Page";
import Loader from "../../components/Loader";
import Carousel from "../../components/Carousel";
import TemperatureCard from "./components/TemperatureCard";
import Filters from "./components/Filters";
import { fetchWeather } from "../../actionCreators/weatherActionCreators";

type StateProps = {|
  city: ?string,
  country: ?string,
  isLoading: boolean,
  days: any[],
|};

type DispatchProps = {|
  fetchWeather: (units?: string) => void,
|};

type HomeProps = {|
  ...$Exact<StateProps>,
  ...$Exact<DispatchProps>,
|};

const groupByDate = (days) => {
  const mappedDays = {};
  days.forEach((day) => {
    const date = day.dt_txt.split(" ")[0];
    if (mappedDays[date]) {
      mappedDays[date].push(day);
    } else {
      mappedDays[date] = [day];
    }
  });

  return mappedDays;
};

const _Home = (props: HomeProps) => {
  const router = useRouter();
  const { units } = router.query;
  const groupedByDay = groupByDate(props.days);
  const onFilter = (value) => () => {
    router.push({ query: { units: value } });
  };

  React.useEffect(() => {
    props.fetchWeather(units);
  }, [units]);

  return (
    <Page title="Weather App | HI">
      {props.isLoading ? (
        <Loader />
      ) : (
        <>
          <Filters onFilterClick={onFilter} />
          <Carousel
            items={Object.keys(groupedByDay)}
            CardContent={TemperatureCard}
            cardProps={{
              groupedByDay,
              country: props.country,
              city: props.city,
            }}
          />
        </>
      )}
    </Page>
  );
};

const redux = [
  (state): StateProps => ({
    days: state.weather.days,
    city: state.weather.currentCity?.name,
    country: state.weather.currentCity?.country,
    isLoading: state.weather.isLoading,
  }),
  (dispatch, ownProps): DispatchProps => ({
    fetchWeather: (units = "Metric") => {
      const defaultOptions = {
        q: "Munich,de",
        cnt: 40,
      };
      dispatch(fetchWeather({ ...defaultOptions, units }));
    },
  }),
];

const Home: React.ComponentType<typeof _Home> = connect(...redux)(_Home);
export default Home;
