import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Loader from "./Loader";

const baseProps = {};
const renderComponent = (props = baseProps) => render(<Loader {...props} />);

describe("<Loader />", () => {
  beforeEach(() => {
    renderComponent();
  });

  describe("when rendering", () => {
    it("renders loader container", () => {
      const loaderContainerElm = screen.getByTestId("loader-container");
      expect(loaderContainerElm).toBeInTheDocument();
    });
    it("renders loader progress", () => {
      const loaderProgressElm = screen.getByTestId("loader-progress");
      expect(loaderProgressElm).toBeInTheDocument();
    });
  });
});
