// @flow
import styled, { type StyledComponent } from "styled-components";

export const CardHeaderContainer: StyledComponent<
  {},
  *,
  HTMLDivElement
> = styled.div`
  margin-bottom: 16px;
`;

export const CardHeader: StyledComponent<{}, *, HTMLHeadingElement> = styled.h3`
  margin: 0;
  color: #5e5e5f;
  font-weight: 500;
`;

export const CardSubHeader: StyledComponent<
  {},
  *,
  HTMLHeadingElement
> = styled.h4`
  margin: 0;
  font-size: 12px;
  font-weight: 500;
  color: #848383;
  padding-top: 4px;
`;

export const CardBody: StyledComponent<{}, *, HTMLDivElement> = styled.div`
  padding: 48px 0;
  text-align: center;
  font-size: 2rem;
`;

export const CardBodySecondaryText: StyledComponent<
  {},
  *,
  HTMLDivElement
> = styled.div`
  font-size: 1rem;
  color: #8f8e8e;

  & > span {
    padding: 0 8px;
  }
`;
