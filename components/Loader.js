// @flow
import * as React from "react";
import styled from "styled-components";
import CircularProgress from "@material-ui/core/CircularProgress";
import { $sizes } from "../styles/variables";

const LoaderContainer = styled.div`
  display: flex;
  justify-content: center;
  padding: ${$sizes.xl};
`;

const Loader = (): React.Node => (
  <LoaderContainer data-testid="loader-container">
    <CircularProgress data-testid="loader-progress" />
  </LoaderContainer>
);

export default Loader;
