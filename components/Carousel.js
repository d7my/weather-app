// @flow
import * as React from "react";
import styled, { type StyledComponent } from "styled-components";
import Card from "@material-ui/core/Card";
import IconButton from "@material-ui/core/IconButton";
import ArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { $sizes, $small, $xSmall, $medium } from "../styles/variables";
import { $smallerThan, $smallerThanQuery } from "../styles/mixins";

type CarouselProps = {|
  cardWidth?: number,
  cardsToShow?: number,
  items: Object[],
  cardProps?: Object,
  CardContent: React.AbstractComponent<any>,
|};

const DEFAULTS = {
  cardsToShow: 3,
  cardWidth: 250,
  position: 0,
};

const CarouselContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const CarouselCardsOuterContainer: StyledComponent<
  {| cardWidth: number, cardsToShow: number |},
  *,
  HTMLDivElement
> = styled.div`
  max-width: ${({ cardWidth, cardsToShow }) => `${cardWidth * cardsToShow}px`};
  overflow: hidden;
  margin-top: ${$sizes.base};
  padding: ${$sizes.lg} 0;

  ${$smallerThan($medium)} {
    padding: ${$sizes.sm} 0;
    max-width: ${({ cardWidth, cardsToShow }) =>
      `${cardsToShow * cardWidth}px`};
  }

  ${$smallerThan($small)} {
    max-width: ${({ cardWidth }) => `${cardWidth}px`};
  }
`;

const CarouselCardsInnerContainer: StyledComponent<
  {| position: number, cardsToShow: number |},
  *,
  HTMLDivElement
> = styled.div`
  display: flex;
  transition: transform 0.3s ease;
  transform: ${({ position, cardsToShow }) => `translateX(${position}px)`};

  ${$smallerThan($small)} {
    padding: ${$sizes.sm} 0;
  }
`;

const CarouselCardContainer = styled.div`
  padding: 0 ${$sizes.base};
`;

const CarouselCard: StyledComponent<
  {| cardWidth: number |},
  *,
  React.ComponentType<typeof Card>
> = styled(({ cardWidth, ...props }) => <Card {...props} />)`
  && {
    background: #ffffff;
    height: 350px;
    min-width: ${({ cardWidth }) => cardWidth - $sizes.numbers.lg}px;
    border: none;
    box-shadow: 0 1px 3px 0 rgba(63, 63, 68, 0.1);
    border-radius: 14px;
    transition: transform 0.2s ease;

    &:hover {
      box-shadow: 0 1px 8px 0 rgba(63, 63, 68, 0.1);
      transform: scale(1.01);
    }

    ${$smallerThan($small)} {
      min-width: ${({ cardWidth }) => cardWidth - $sizes.numbers.lg}px;
    }
  }
`;

const ArrowContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 0 ${$sizes.base};
`;

const Carousel = (props: CarouselProps): React.Node => {
  const { CardContent } = props;
  const cardWidth = props.cardWidth || DEFAULTS.cardWidth;
  const [position, setPosition] = React.useState<number>(DEFAULTS.position);
  const [cardsToShow, setCardsToShow] = React.useState<number>(
    props.cardsToShow || DEFAULTS.cardsToShow
  );
  const maximumPosition = -(
    cardWidth * props.items.length -
    cardsToShow * cardWidth
  );
  const moveForward = () =>
    setPosition((prevPosition) =>
      prevPosition > maximumPosition ? prevPosition - cardWidth : prevPosition
    );
  const moveBackward = () =>
    setPosition((prevPosition) =>
      prevPosition === DEFAULTS.position
        ? prevPosition
        : prevPosition + cardWidth
    );

  React.useEffect(() => {
    const keydownHandler = (event: KeyboardEvent) => {
      if (event.key === "ArrowRight") {
        moveForward();
      } else if (event.key === "ArrowLeft") {
        moveBackward();
      }
    };
    const resizeHandler = () => {
      const isSmallerThanMedium = window.matchMedia($smallerThanQuery($medium));
      const isSmallerThanSmall = window.matchMedia($smallerThanQuery($small));

      if (isSmallerThanSmall.matches) {
        setCardsToShow(1);
      } else if (isSmallerThanMedium.matches) {
        setCardsToShow(2);
      } else {
        setCardsToShow(cardsToShow);
      }

      setPosition(DEFAULTS.position);
    };
    window.addEventListener("resize", resizeHandler);
    document.addEventListener("keydown", keydownHandler);

    return () => {
      window.removeEventListener("resize", resizeHandler);
      document.removeEventListener("keydown", keydownHandler);
    };
  }, []);

  return (
    <CarouselContainer data-testid="carousel-container">
      <ArrowContainer>
        <IconButton
          disabled={position === 0}
          onClick={moveBackward}
          aria-label="Previous cards"
          data-testid="carousel-previous-button"
        >
          <ArrowLeft />
        </IconButton>
      </ArrowContainer>
      <CarouselCardsOuterContainer
        cardWidth={cardWidth}
        cardsToShow={cardsToShow}
      >
        <CarouselCardsInnerContainer
          cardsToShow={cardsToShow}
          position={position}
        >
          {props.items.map((item, index) => (
            <CarouselCardContainer key={index}>
              <CarouselCard
                cardWidth={cardWidth}
                data-testid={`carousel-card-${index}`}
              >
                <CardContent item={item} cardProps={props.cardProps} />
              </CarouselCard>
            </CarouselCardContainer>
          ))}
        </CarouselCardsInnerContainer>
      </CarouselCardsOuterContainer>
      <ArrowContainer>
        <IconButton
          disabled={position <= maximumPosition}
          onClick={moveForward}
          aria-label="Next cards"
          data-testid="carousel-next-button"
        >
          <ArrowRight />
        </IconButton>
      </ArrowContainer>
    </CarouselContainer>
  );
};

export default Carousel;
