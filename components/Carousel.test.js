import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Carousel from "./Carousel";

const CardContent = ({ item }) => (
  <div data-testid={`content-${item}`}>{item}</div>
);

const baseProps = {
  items: ["1", "2", "3"],
  CardContent,
};

const renderComponent = (props = baseProps) => render(<Carousel {...props} />);

describe("<Carousel />", () => {
  describe("when rendering <Carousel /> component", () => {
    beforeEach(() => {
      renderComponent();
    });

    it("renders carousel container", () => {
      const carouselContainerElm = screen.getByTestId("carousel-container");
      expect(carouselContainerElm).toBeInTheDocument();
    });

    it("renders carousel previous button", () => {
      const carouselPrevButtonElm = screen.getByTestId(
        "carousel-previous-button"
      );
      expect(carouselPrevButtonElm).toBeInTheDocument();
    });

    it("renders carousel next button", () => {
      const carouselNextButtonElm = screen.getByTestId("carousel-next-button");
      expect(carouselNextButtonElm).toBeInTheDocument();
    });

    it("renders items cards", () => {
      baseProps.items.forEach((item) => {
        const itemElm = screen.getByTestId(`content-${item}`);
        expect(itemElm).toBeInTheDocument();
      });
    });
  });
});
