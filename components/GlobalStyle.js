// @flow
import * as React from "react";
import { createGlobalStyle, type StyledComponent } from "styled-components";

const GlobalStyle: React.ComponentType<{}> = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background-color: #f9f9f9;
    font-family: 'IBM Plex Sans', sans-serif;
    box-sizing: border-box;
  }
`;

export default GlobalStyle;
