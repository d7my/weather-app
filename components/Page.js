// @flow
import * as React from "react";
import Head from "next/head";
import styled from "styled-components";
import { $sizes } from "../styles/variables";

type PageProps = {
  children: React.Node,
  title: string,
};

const Section = styled.section`
  padding-top: ${$sizes.xl};
`;

const Page = (props: PageProps): React.Element<any> => (
  <Section>
    <Head>
      <title>{props.title}</title>
    </Head>
    {props.children}
  </Section>
);

export default Page;
