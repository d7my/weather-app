// @flow

export const $largerThanQuery = ($breakPoint: number): string =>
  `(min-width: ${$breakPoint}px)`;
export const $largerThan = ($breakPoint: number): string =>
  `@media ${$largerThanQuery($breakPoint)}`;

export const $smallerThanQuery = ($breakPoint: number): string =>
  `(max-width: ${$breakPoint}px)`;
export const $smallerThan = ($breakPoint: number): string =>
  `@media ${$smallerThanQuery($breakPoint)}`;

export const $betweenWidth = (
  $minGridBreakPoint: number,
  $maxGridBreakPoint: number
): string =>
  `@media (min-width: ${$minGridBreakPoint}px and max-width: ${$maxGridBreakPoint}px)`;
