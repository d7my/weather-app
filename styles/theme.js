import { createMuiTheme } from "@material-ui/core/styles";

export const $theme = createMuiTheme({
  palette: {
    primary: {
      light: "#BBDEFB",
      main: "#2196F3",
      dark: "#1976D2",
    },
  },
  status: {
    danger: "orange",
  },
});
