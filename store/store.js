// @flow
import { useMemo } from "react";
import { createStore, applyMiddleware, type Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import promiseMiddleware from "redux-promise-middleware";
import reducers from "../reducers";
import { APP_ID } from "../constants/config";
import type { ApplicationState } from "../reducers/initialState";

let store;

const middleWares = [
  promiseMiddleware,
  thunkMiddleware.withExtraArgument({ urlConfigs: { APPID: APP_ID } }),
];

const initStore = (
  initialState: ApplicationState
): Store<ApplicationState, *> =>
  createStore(
    reducers,
    initialState,
    composeWithDevTools(applyMiddleware(...middleWares))
  );

export const initializeStore = (
  preloadedState: ApplicationState
): Store<ApplicationState, *> => {
  let _store = store ?? initStore(preloadedState);

  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    });
    store = undefined;
  }

  if (typeof window === "undefined") return _store;
  if (!store) store = _store;

  return _store;
};

export const useStore = (
  initialState: ApplicationState
): Store<ApplicationState, *> => {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
};
